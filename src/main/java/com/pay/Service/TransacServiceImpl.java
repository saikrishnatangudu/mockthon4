package com.pay.Service;

import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.pay.Dto.TransactionDto;
import com.pay.Dto.TransactionResponseDto;
import com.pay.Exception.UserNotfoundException;
import com.pay.Model.Transaction;
import com.pay.Model.User;
import com.pay.repository.TransactionRepository;
import com.pay.repository.UserRepository;

@Service
public class TransacServiceImpl implements TransacService{
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	UserRepository userRepository;
	@Autowired
	TransactionRepository transactionRepository;

	@Bean
	@LoadBalanced
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Override
	public TransactionResponseDto createTransac(TransactionDto transactionDto) {
		
		Optional<User> user1 =userRepository.findByPhone(transactionDto.getPhoneNo());
		User user = null;
		if(user1.isPresent()) {
			user= user1.get();
		}else
		{
			throw new UserNotfoundException("user is not registerd in phonePay");
		}
	Transaction transaction = new Transaction();
		BeanUtils.copyProperties(transactionDto, transaction);
		transaction.setUser(user);
		try {
			this.transction(transactionDto);
		} catch (Exception e) {
			throw new UserNotfoundException("Transaction is failure");
		}
		
	Transaction transaction2=	transactionRepository.save(transaction);
	TransactionResponseDto transactionResponseDto= new TransactionResponseDto();
		BeanUtils.copyProperties(transaction2, transactionResponseDto);
		transactionResponseDto.setMessage("Transaction is sucessfull");
		
		return transactionResponseDto;
	}
	
	
	public ResponseEntity<String> transction(TransactionDto transactionDto) {
		String uri = "http://BANKING-SERVICE/payment";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("phoneNo", transactionDto.getPhoneNo());
		request.put("amount", transactionDto.getAmount());
		request.put("toPhoneNo", transactionDto.getToPhoneNo());

		HttpEntity<String> entity = new HttpEntity<>(request.toString(), headers);

		
		return restTemplate.postForEntity(uri, entity, String.class);
	}



	
	
	
	
	
	
	

}
