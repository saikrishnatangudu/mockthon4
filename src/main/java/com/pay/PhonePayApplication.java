package com.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PhonePayApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhonePayApplication.class, args);
	}

}
